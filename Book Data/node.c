//
//  node.c
//  Book Data
//
//  Created by user on 10/8/19.
//  Copyright © 2019 user. All rights reserved.
//
#include <stdlib.h>
#include <stdio.h>
#include "node.h"


struct Node* head;
struct Node* last;
struct Node* currentNode;
struct firstSecond {
    struct Node* current;
    struct Node* beforeCurrent;
};
typedef struct firstSecond Struct;

struct Node* getNewNode(int key, char *string) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->key = key;
    newNode->data = string;
    newNode->next = NULL;
    newNode->prev = NULL;
    
    return newNode;
}

struct Node* getNextNode(struct Node* current) {
    if (current->next == NULL) {
        printf("You're in the end of the list. This is the last item:\n");
        return last;
    }
    currentNode = current->next;
    return current->next;
}

struct Node* getPrevNode(struct Node* current) {
    if (current->prev == NULL) {
        printf("You're in the start of the list. This is the first item:\n");
        return head;
    }
    currentNode = current->prev;
    return current->prev;
}

void insertAtHead(int key, char *string) {
    struct Node* newNode = getNewNode(key, string);
    
    if (head == NULL) {
        head = newNode;
        return;
    }
    head->prev = newNode;
    newNode->next = head;
    head = newNode;
}

void insertAtTail(int key, char *string) {
    struct Node* temp = head;
    struct Node* newNode = getNewNode(key, string);
    if (head == NULL) {
        head = newNode;
        return;
    }
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
    newNode->prev = temp;
    last = newNode;
}

void connectNodes(struct Node* first,struct Node* second) {
    first->next = second;
    second->prev = first;
}

int areTheyNeighbours(struct Node* first,struct Node* second) {
    return ( first->next == second && second->prev == first ) || ( first->prev == second && second->next == first );
}

void refreshOuterPointers(struct Node* A) {
    if (A->prev != NULL) {
        A->prev->next = A;
    } else {
        head = A;
    }

    if (A->next != NULL) {
        A->next->prev = A;
    } else {
        last = A;
    }
}

int getListSize() {
    struct Node* temp = head;
    int i = 0;
    while (temp != NULL) {
        temp = temp->next;
        i++;
    }
    return i;
}

Struct* searchNode(int key) {
    struct Node* p;
    struct Node* prev = NULL;
    Struct* s = (Struct*)malloc(sizeof(head));
    p = head;
    
    while (p!= NULL && p->key != key) {
        prev = p;
        p = p->next;
    }
    s->current = p;
    s->beforeCurrent = prev;
    return s;
}

void swapNodes(int key1, int key2) {
    int size = getListSize();

    if (key1 < 1 || key1 > size || key2 < 1 || key2 > size) {
        printf("Invalid index. Please type an index between 1 and %d.", size);
        return;
    }
    if (key1 == key2) {
        return;
    }
    Struct* s1 = searchNode(key1);
    Struct* s2 = searchNode(key2);
    struct Node* first = s1->current;
    struct Node* second = s2->current;
    struct Node* swapperVector[4];
    struct Node* temp;
    int tempKey = -1;
    
    
    if (second->next == first) {
        temp = first;
        first = second;
        second = temp;
    }

    swapperVector[0] = first->prev;
    swapperVector[1] = second->prev;
    swapperVector[2] = first->next;
    swapperVector[3] = second->next;

    if (areTheyNeighbours(first,second)) {
        first->prev = swapperVector[2];
        second->prev = swapperVector[0];
        first->next = swapperVector[3];
        second->next = swapperVector[1];
    } else {
        first->prev = swapperVector[1];
        second->prev = swapperVector[0];
        first->next = swapperVector[3];
        second->next = swapperVector[2];
    }
    
    if (s1->beforeCurrent == NULL) {
        first->prev->next = first;
    }
    if (s2->beforeCurrent == NULL) {
        first->next = second->prev;
    }
    if (s1->beforeCurrent != NULL && s2->beforeCurrent != NULL) {
        first->prev->next = second;
        second->prev->next = first;
    }
    
    refreshOuterPointers(first);
    refreshOuterPointers(second);
    tempKey = first->key;
    first->key = second->key;
    second->key = tempKey;
}

void rearangeIndexes(struct Node* current, int oldKey) {
    struct Node* temp = current;
    while (temp != NULL) {
        temp->key = oldKey;
        temp = temp->next;
        oldKey++;
    }
}

void deleteNode(int key) {
    Struct* s = searchNode(key);
    struct Node* node = s->current;
    
    if (head == node) {
        head = node->next;
    }
    if (last == node) {
        last = node->prev;
    }
    if (node->next != NULL) {
        node->next->prev = node->prev;
    }
    if (node->prev != NULL) {
        node->prev->next = node->next;
    }
    rearangeIndexes(node->next, node->key);
    free(node);
    return;
}
