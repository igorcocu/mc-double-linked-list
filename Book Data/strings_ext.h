//
//  strings_ext.h
//  Book Data
//
//  Created by user on 10/15/19.
//  Copyright © 2019 user. All rights reserved.
//

#ifndef strings_ext_h
#define strings_ext_h

#include <stdio.h>

int compareStrings(char a[], char b[]);
int stringLength(char s[]);

#endif /* strings_ext_h */
