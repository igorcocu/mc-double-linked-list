//
//  strings_ext.c
//  Book Data
//
//  Created by user on 10/15/19.
//  Copyright © 2019 user. All rights reserved.
//

#include "strings_ext.h"


int compareStrings(char a[], char b[]) {
   int c = 0;
   while (a[c] == b[c]) {
      if (a[c] == '\0' || b[c] == '\0')
         break;
      c++;
   }
   if (a[c] == '\0' && b[c] == '\0')
      return 0;
   else
      return -1;
}

int stringLength(char s[]) {
   int c = 0;
   while (s[c] != '\0')
      c++;
   return c;
}
