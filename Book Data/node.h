//
//  node.h
//  Book Data
//
//  Created by user on 10/8/19.
//  Copyright © 2019 user. All rights reserved.
//

#ifndef node_h
#define node_h

#include <stdlib.h>

struct Node {
    char *data;
    int key;
    struct Node* next;
    struct Node* prev;
};

extern struct Node* head;
extern struct Node* last;
extern struct Node* currentNode;

struct Node* getNewNode(int key, char *string);
struct Node* getNextNode(struct Node* current);
struct Node* getPrevNode(struct Node* current);
void insertAtHead(int key, char *string);
void insertAtTail(int key, char *string);
void swapNodes(int key1, int key2);
void deleteNode(int key);
int getListSize(void);
#endif /* node_h */
