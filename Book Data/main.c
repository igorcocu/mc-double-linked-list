//
//  main.c
//  Book Data
//
//  Created by user on 10/7/19.
//  Copyright © 2019 user. All rights reserved.
//

#include <stdio.h>
#include "node.h"
#include "main.h"
#include "strings_ext.h"

void createList(void);
void printList(void);
void printFirstItem(void);
void reversePrint(void);
void showNavigationOptions(void);
void handleNavigation(int);
char * getFormatedArray(char arr[]);
void showInputOption(void);
int key = 1;

int main(int argc, const char * argv[]) {
    head = NULL;
    createList();
    currentNode = head;
    showNavigationOptions();
    return 0;
}

void createList() {
    insertAtTail(key++, "1984 - George Orwell");
    insertAtTail(key++, "Guns, germs and steel - Jared Diamond");
    insertAtTail(key++, "Pride and Prejudice - Jane Austen");
    insertAtTail(key++, "Catch 22 - Joseph Heller");
    insertAtTail(key++, "A brief history of time - Stephen Hawking");
}

void printList() {
    struct Node* temp = head;
    int i = 1;
    printf("\n");
    printf("DATA IN THE LIST:\n");
    while (temp != NULL) {
        printf("%d. %s\n", temp->key, temp->data);
        temp = temp->next;
        i++;
    }
    printf("\n");
}

void showNavigationOptions() {
    printf("============================================\n");
    printf("DOUBLY LINKED LIST PROGRAM\n");
    printf("============================================\n");
    printf("1. Navigate to next item.\n");
    printf("2. Navigate to previous item.\n");
    printf("3. Move items between each other.\n");
    printf("4. Insert a new item.\n");
    printf("5. Delete item at position.\n");
    printf("6. Display list.\n");
    printf("0. Exit.\n");
    printf("\n");

    showInputOption();
}

void showInputOption() {
    printf("--------------------------------------------\n");
    printf("Enter your choice : ");
    int choice = -1;
    scanf("%d", &choice);
    handleNavigation(choice);
    printf("\n");
}

void handleNavigation(int choice) {
    int key1, key2, keyDel;
    char title[1000];

        switch (choice) {
            case 1:
                printf("Next item is:\n");
                struct Node* next = getNextNode(currentNode);
                printf("%d. %s\n", next->key, next->data);
                break;
            case 2:
                printf("Previous item is:\n");
                struct Node* prev = getPrevNode(currentNode);
                printf("%d. %s\n", prev->key, prev->data);
                break;
            case 3:
                printf("Type the index of the first item to be moved:\n");
                scanf("%d", &key1);
                printf("Type the index of the second item:\n");
                scanf("%d", &key2);
                swapNodes(key1, key2);
                printList();
                break;
            case 4:
                printf("Type the title of the book:\n");
                fgets(title, sizeof(title), stdin);
                insertAtTail(key++, title);
                printList();
                break;
            case 5:
                printf("Type the index of the item to be deleted:\n");
                scanf("%d", &keyDel);
                deleteNode(keyDel);
                key = getListSize();
                printList();
            case 6:
                printList();
                break;
            case 0:
                printf("Good buy. Have a nice day. Hope to see you again.");
                return;
            default:
                printf("Error! Invalid choice. Please choose between 0-6");
        }
    printf("\n");
    printf("\n");
    printf("\n");
    showNavigationOptions();
}

void reversePrint() {
    struct Node* temp = head;
    if (temp == NULL) {
        return;
    }
    while (temp->next != NULL) {
        temp = temp->next;
    }
    printf("Reverse: ");
    while (temp != NULL) {
        printf("%s\n", temp->data);
        temp = temp->prev;
    }
    printf("\n");
}

void printFirstItem() {
    struct Node* temp = head;
    
    if (temp != NULL) {
        printf("This is the first item from our list:\n");
        printf("%s\n", currentNode->data);
    }
    printf("\n");
    showNavigationOptions();
}
